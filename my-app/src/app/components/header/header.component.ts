import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public logo: string = 'https://cdn.dribbble.com/users/2091812/screenshots/8269090/media/b2792d4cf9f0bca24d45c9acb89d38f8.gif';
  public logoAlt: string = 'Logo mixr';
  public name: string = 'MIXER';


  constructor() {
  }

  ngOnInit(): void {
  }

}

