import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  facebook: string;
  instagram: string;
  whatsapp: string;
  youtube: string;
  mail: string;

  constructor() {
    this.facebook = 'Facebook';
    this.instagram = 'Instagram';
    this.whatsapp = 'Whatsapp';
    this.youtube = 'Youtube';
    this.mail = 'Gmail';
  }

  ngOnInit(): void {
  }

}