import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailNonAlcComponent } from './detail-non-alc.component';

describe('DetailNonAlcComponent', () => {
  let component: DetailNonAlcComponent;
  let fixture: ComponentFixture<DetailNonAlcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailNonAlcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailNonAlcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
