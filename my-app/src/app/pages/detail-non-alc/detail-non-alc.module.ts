import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailNonAlcRoutingModule } from './detail-non-alc-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DetailNonAlcRoutingModule
  ]
})
export class DetailNonAlcModule { }
