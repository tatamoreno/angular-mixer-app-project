import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NonAlcoholicComponent } from './non-alcoholic-component/non-alcoholic.component';

const routes: Routes = [{ path: '', component: NonAlcoholicComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NonAlcoholicRoutingModule { }
