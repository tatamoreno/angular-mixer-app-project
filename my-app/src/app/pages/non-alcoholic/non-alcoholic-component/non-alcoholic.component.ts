import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { DrinksListInterface } from 'src/app/models/models-interface';
import { FakeApiService } from 'src/app/services/fake-api/fake-api.service';


@Component({
  selector: 'app-non-alcoholic',
  templateUrl: './non-alcoholic.component.html',
  styleUrls: ['./non-alcoholic.component.scss']
})
export class NonAlcoholicComponent implements OnInit {  
  public nonAlcoholicDrinkList: DrinksListInterface[] | any = [];

  constructor(private fakeApiService: FakeApiService, private location: Location) { }

  ngOnInit(): void {
    this.getDrinksList()
  }

  public getDrinksList(): void {
    this.fakeApiService.getDrinksList('http://localhost:3000/nonAlcoholicDrinksList').subscribe(
      (data: DrinksListInterface[]) => {
      this.nonAlcoholicDrinkList = data;
      
      console.log(data);
    }),
    (err: { message: any; }) => {
      console.error(err.message);
    }
  }

  public goBack(): void{
    this.location.back();
  }

}
