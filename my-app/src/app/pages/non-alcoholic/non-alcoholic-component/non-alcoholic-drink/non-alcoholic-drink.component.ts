import { Component, OnInit, Input } from '@angular/core';
import { DrinksListInterface } from 'src/app/models/models-interface';

@Component({
  selector: 'app-non-alcoholic-drink',
  templateUrl: './non-alcoholic-drink.component.html',
  styleUrls: ['./non-alcoholic-drink.component.scss']
})
export class NonAlcoholicDrinkComponent implements OnInit {
  @Input() nonAlcoholicDrinkList: DrinksListInterface[] | any = [];

  constructor() { }

  ngOnInit(): void {
  }

}
