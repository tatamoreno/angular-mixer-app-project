import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonAlcoholicDrinkComponent } from './non-alcoholic-drink.component';

describe('NonAlcoholicDrinkComponent', () => {
  let component: NonAlcoholicDrinkComponent;
  let fixture: ComponentFixture<NonAlcoholicDrinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonAlcoholicDrinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NonAlcoholicDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
