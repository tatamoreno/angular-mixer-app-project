import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NonAlcoholicRoutingModule } from './non-alcoholic-routing.module';
import { NonAlcoholicComponent } from './non-alcoholic-component/non-alcoholic.component';
import { NonAlcoholicDrinkComponent } from './non-alcoholic-component/non-alcoholic-drink/non-alcoholic-drink.component';


@NgModule({
  declarations: [NonAlcoholicComponent, NonAlcoholicDrinkComponent],
  imports: [
    CommonModule,
    NonAlcoholicRoutingModule
  ]
})
export class NonAlcoholicModule { }
