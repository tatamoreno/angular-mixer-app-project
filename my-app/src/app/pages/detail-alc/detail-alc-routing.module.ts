import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailAlcComponent } from './detail-alc-component/detail-alc.component';

const routes: Routes = [{ path: '', component: DetailAlcComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailAlcRoutingModule { }
