import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailAlcRoutingModule } from './detail-alc-routing.module';
import { DetailAlcComponent } from './detail-alc-component/detail-alc.component';
import { AlcDrinkDetailComponent } from './detail-alc-component/alc-drink-detail/alc-drink-detail.component';


@NgModule({
  declarations: [AlcDrinkDetailComponent, DetailAlcComponent],
  imports: [
    CommonModule,
    DetailAlcRoutingModule
  ]
})
export class DetailAlcModule { }
