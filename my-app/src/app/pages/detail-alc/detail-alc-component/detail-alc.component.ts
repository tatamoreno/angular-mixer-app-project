import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { FakeApiService } from './../../../services/fake-api/fake-api.service';
import { DrinksDetailList } from '../../../models/models-interface'

@Component({
  selector: 'app-detail-alc',
  templateUrl: './detail-alc.component.html',
  styleUrls: ['./detail-alc.component.scss']
})
export class DetailAlcComponent implements OnInit {
  public detail: DrinksDetailList | any;
  private drinkId: number | any;

  constructor(
    private fakeApiService: FakeApiService,
    private location: Location, 
    private route: ActivatedRoute 
    ) { }

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): void{
      this.route.paramMap.subscribe(params => {
        this.drinkId = params.get('id');
        console.log(this.drinkId);
      });
      this.fakeApiService.getDetail(this.drinkId).subscribe(
        (data: DrinksDetailList | any) => {
          console.log(data);
        this.detail = data;
      
      }, 
      (err) => {
        console.error(err.message)
      }
    );
  }

  public goBack(): void{
    this.location.back();
  }

}
