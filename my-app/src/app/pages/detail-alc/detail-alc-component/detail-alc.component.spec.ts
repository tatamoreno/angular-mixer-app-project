import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAlcComponent } from './detail-alc.component';

describe('DetailAlcComponent', () => {
  let component: DetailAlcComponent;
  let fixture: ComponentFixture<DetailAlcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailAlcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAlcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
