import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlcDrinkDetailComponent } from './alc-drink-detail.component';

describe('AlcDrinkDetailComponent', () => {
  let component: AlcDrinkDetailComponent;
  let fixture: ComponentFixture<AlcDrinkDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlcDrinkDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlcDrinkDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
