import { Component, OnInit, Input } from '@angular/core';
import { DrinksDetailList } from 'src/app/models/models-interface';

@Component({
  selector: 'app-alc-drink-detail',
  templateUrl: './alc-drink-detail.component.html',
  styleUrls: ['./alc-drink-detail.component.scss']
})
export class AlcDrinkDetailComponent implements OnInit {

  constructor() { }
  @Input() detail: DrinksDetailList | any;
  ngOnInit(): void {
  }

}
