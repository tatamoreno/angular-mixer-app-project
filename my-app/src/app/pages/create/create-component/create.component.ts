import { FakeApiService } from '../../../services/fake-api/fake-api.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DrinksListInterface } from 'src/app/models/models-interface';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public createForm: FormGroup | any = null;
  public submitted: boolean = false;

  constructor(
    private formBuilder:FormBuilder, 
    private fakeApiService:FakeApiService,) { }

  ngOnInit(): void {
    this.createDrinkForm();
  }

  public createDrinkForm():void {
    this.createForm = this.formBuilder.group({
      id: ['', [Validators.required]],
      strDrink: ['', [Validators.required]],
      strDrinkThumb: ['', [Validators.required]],
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    if(this.createForm.valid) {
      this.postDrink();
      this.createForm.reset();
      this.submitted = false;
    }
  }

  public postDrink(): void {
    const drink: DrinksListInterface = {
      id: this.createForm.get('id').value,
      strDrink: this.createForm.get('strDrink').value,
      strDrinkThumb: this.createForm.get('strDrinkThumb').value,
    };
    this.fakeApiService.postNewAlcDrink(drink).subscribe(
      (data: any) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    )
  }
}
