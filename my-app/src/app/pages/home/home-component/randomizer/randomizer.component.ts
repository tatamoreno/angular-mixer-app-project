import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-randomizer',
  templateUrl: './randomizer.component.html',
  styleUrls: ['./randomizer.component.scss']
})
export class RandomizerComponent implements OnInit {
  @Input() randomDrink: any | null;
  constructor() { 
  }

  ngOnInit(): void {
    console.log(this.randomDrink);
    
  }

}
