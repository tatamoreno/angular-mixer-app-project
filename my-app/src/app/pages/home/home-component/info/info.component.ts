import { Info } from './../../../../models/ihome';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  @Input() info: Info | any;
  constructor() { }

  ngOnInit(): void {
  }

}
