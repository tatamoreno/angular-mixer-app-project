import { Component, OnInit } from '@angular/core';
import { TheCocktailDBService } from '../../../services/theCocktailDB/the-cocktail-db.service';
import { Info } from 'src/app/models/ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public info: Info = {
    infoText1: 'are a form of art, one of the greatest expressions of human creativity applied to the world of spirits, liquors and juices. Today with this Dharmag we want to inaugurate a new chapter, we want to open a window on the world of cocktails to explore, learn and prepare the best cocktail recipes ever made. We have already published a lot of recipes, but we will also offer you to create your brand new cocktail born from the love for experimentation.',
    infoText2: 'We say it now as advice: do not overdo it, a couple of drink are ok, but no more. Never! Drinking is a pleasure that must be sipped slowly to savor every nuance, suggestions and flavors of spirits and liqueurs that combine to create ethereal symphonies, otherwise it becomes a sad, hydraulic exercise of repetition. And it doesn’t “juvant” in this case.',
  }

  constructor(private theCocktailDBService: TheCocktailDBService ) { }
  public randomDrink: any | null = null;
  ngOnInit(): void {
    this.theCocktailDBService.getRandomDrink().subscribe(
    (result: any) => {
      this.randomDrink = result.drinks[0];
      console.log(result.drinks[0]);
    },
    (err: { message: any; }) => {
      console.error(err.message);
    }
  );
}
    
}
