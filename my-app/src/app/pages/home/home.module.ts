import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { InfoComponent } from '././home-component/info/info.component';
import { RandomizerComponent } from './home-component/randomizer/randomizer.component';


@NgModule({
  declarations: [HomeComponent, InfoComponent, RandomizerComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
