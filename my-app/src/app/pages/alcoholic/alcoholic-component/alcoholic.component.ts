import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { FakeApiService } from './../../../services/fake-api/fake-api.service';
import { DrinksListInterface } from '../../../models/models-interface';

@Component({
  selector: 'app-alcoholic',
  templateUrl: './alcoholic.component.html',
  styleUrls: ['./alcoholic.component.scss']
})
export class AlcoholicComponent implements OnInit {
    public alcoholicDrinkList: DrinksListInterface[] | any = [];

  constructor(private fakeApiService: FakeApiService, private location: Location) { }

  ngOnInit(): void {
    this.getDrinksList()
  }

   public getDrinksList(): void {
     
    this.fakeApiService.getDrinksList('http://localhost:3000/alcoholicDrinksList').subscribe(
      (data: DrinksListInterface[]) => {
      this.alcoholicDrinkList = data;
      
      console.log(data);
    }),
    (err: { message: any; }) => {
      console.error(err.message);
    }
  }

  public goBack(): void{
    this.location.back();
  }
}
