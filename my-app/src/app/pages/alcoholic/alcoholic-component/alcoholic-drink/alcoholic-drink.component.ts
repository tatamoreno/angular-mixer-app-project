import { Component, OnInit, Input } from '@angular/core';
import { DrinksListInterface } from '../../../../models/models-interface';

@Component({
  selector: 'app-alcoholic-drink',
  templateUrl: './alcoholic-drink.component.html',
  styleUrls: ['./alcoholic-drink.component.scss']
})
export class AlcoholicDrinkComponent implements OnInit {
  @Input() alcoholicDrinkList: DrinksListInterface | any;
  constructor() { }

  ngOnInit(): void {
  }

}
