import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlcoholicDrinkComponent } from './alcoholic-drink.component';

describe('AlcoholicDrinkComponent', () => {
  let component: AlcoholicDrinkComponent;
  let fixture: ComponentFixture<AlcoholicDrinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlcoholicDrinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlcoholicDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
