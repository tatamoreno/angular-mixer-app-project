import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlcoholicComponent } from './alcoholic-component/alcoholic.component';

const routes: Routes = [{ path: '', component: AlcoholicComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlcoholicRoutingModule { }
