import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlcoholicRoutingModule } from './alcoholic-routing.module';
import { AlcoholicComponent } from './alcoholic-component/alcoholic.component';
import { AlcoholicDrinkComponent } from './alcoholic-component/alcoholic-drink/alcoholic-drink.component';


@NgModule({
  declarations: [AlcoholicComponent, AlcoholicDrinkComponent],
  imports: [
    CommonModule,
    AlcoholicRoutingModule
  ]
})
export class AlcoholicModule { } 
