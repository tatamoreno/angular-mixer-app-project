import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { TheCocktailDBService } from './services/theCocktailDB/the-cocktail-db.service';
import { FakeApiService } from './services/fake-api/fake-api.service';
import { DetailAlcComponent } from './pages/detail-alc/detail-alc-component/detail-alc.component';
import { DetailNonAlcComponent } from './pages/detail-non-alc/detail-non-alc-component/detail-non-alc.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DetailNonAlcComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [TheCocktailDBService, FakeApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
