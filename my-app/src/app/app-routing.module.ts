import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'alc',
    loadChildren: () =>
      import('./pages/alcoholic/alcoholic.module').then((m) => m.AlcoholicModule),
  },
  {
    path: 'detailalc/:id',
    loadChildren: () =>
      import('./pages/detail-alc/detail-alc.module').then((m) => m.DetailAlcModule),
  },
  {
    path: 'nonalc',
    loadChildren: () =>
      import('./pages/non-alcoholic/non-alcoholic.module').then((m) => m.NonAlcoholicModule),
  },
  {
    path: 'detailnonalc/:id',
    loadChildren: () =>
      import('./pages/detail-non-alc/detail-non-alc.module').then((m) => m.DetailNonAlcModule),
  },
  {
    path: 'create',
    loadChildren: () =>
      import('./pages/create/create.module').then((m) => m.CreateModule),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./pages/about/about.module').then((m) => m.AboutModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
