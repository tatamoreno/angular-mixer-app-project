// API response:
export interface ResponseApiTheCocktailDB{
    drinks: ResponseApiTheCocktailDBList[];
}
export interface ResponseApiTheCocktailDBList{
    strDrink: string;
    strDrinkThumb: string;
    idDrink: string;
}
// API response random:
export interface ResponseApiTheCocktailDBRandom{
    drinks: ApiRandomDrink[];
}
export interface ApiRandomDrink{
    idDrink: string;
    strDrink: string;
    strDrinkAlternate: string | null;
    strDrinkES: string | null;
    strDrinkDE: string | null;
    strDrinkFR: string | null;
    strDrinkZHHANS: string | null;
    strDrinkZHHANT: string | null;
    strTags: string | null;
    strVideo: string | null;
    strCategory: string;
    strIBA: string | null;
    strAlcoholic: string;
    strGlass: string;
    strInstructions: string;
    strInstructionsES: string | null;
    strInstructionsDE: string;
    strInstructionsFR: string | null;
    strInstructionsZHHANS: string | null;
    strInstructionsZHHANT: string | null;
    strDrinkThumb: string;
    strIngredient1: string | null;
    strIngredient2: string | null;
    strIngredient3: string | null;
    strIngredient4: string | null;
    strIngredient5: string | null;
    strIngredient6: string | null;
    strIngredient7: string | null;
    strIngredient8: string | null;
    strIngredient9: string | null;
    strIngredient10: string | null;
    strIngredient11: string | null;
    strIngredient12: string | null;
    strIngredient13: string | null;
    strIngredient14: string | null;
    strIngredient15: string | null;
    strMeasure1: string | null;
    strMeasure2: string | null;
    strMeasure3: string | null;
    strMeasure4: string | null;
    strMeasure5: string | null;
    strMeasure6: string | null;
    strMeasure7: string | null;
    strMeasure8: string | null;
    strMeasure9: string | null;
    strMeasure10: string | null;
    strMeasure11: string | null;
    strMeasure12: string | null;
    strMeasure13: string | null;
    strMeasure14: string | null;
    strMeasure15: string | null;
    strImageSource: string | null;
    strImageAttribution: string | null;
    strCreativeCommonsConfirmed: string | null;
    dateModified: Date;
}
export interface ResponseApiTheCocktailDBFormattedRandom{
    drinks: ApiFormattedRandomDrink[];
}
export interface ApiFormattedRandomDrink{
    idDrink: string;
    strDrink: string;
    strDrinkThumb: string;
    strCategory: string;
    strAlcoholic: string;
    strGlass: string;
    strInstructions: string;
    strInstructionsDE?: string;
}

// FAKE API:

export interface DrinksListInterface{
    id: number;
    strDrink: string;
    strDrinkThumb: string;
}

export interface DrinksDetailList {
    drinksDetailList: DrinkDetail[];
}

export interface DrinkDetail{
    id: number;
    strDrink: string;
    strDrinkThumb: string;
    strCategory: string;
    strAlcoholic: string;
    strGlass: string;
    strInstructions: string;
    strInstructionsDE: string;
    strIngredients: string[];
    strMeasures: string[];
}

