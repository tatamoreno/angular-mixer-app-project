import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { DrinksDetailList, DrinksListInterface } from '../../models/models-interface';


@Injectable({
  providedIn: 'root'
})
export class FakeApiService {

  private fakeApiAlcListUrl = 'http://localhost:3000/alcoholicDrinksList';
  private fakeApinonAlcListUrl = 'http://localhost:3000/nonAlcoholicDrinksList';
  private fakeApiAlcDetailUrl = 'http://localhost:3000/alcoholicDetailsList';
  private FakeApiNonAlcDetailUrl = 'http://localhost:3000/nonAlcoholicDetailsList';

  // public DrinksList: DrinksList[] = [];
  constructor(private http: HttpClient) { /*empty*/ }

  // DRINKS LIST:

  public getDrinksList(url: string): Observable<DrinksListInterface[]> {
    return this.http.get(url).pipe(
      map((response: DrinksListInterface[] | any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  // ALCOHOLIC DRINK DETAIL:

  public getDetail(id: string): Observable<any> {
    return this.http.get(`${this.fakeApiAlcDetailUrl}/${id}`).pipe(
      map((response) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public postNewAlcDrink(drinksListInterface:DrinksListInterface): Observable<any> {
    return this.http.post(this.fakeApiAlcListUrl, drinksListInterface).pipe(
      map((response: DrinksListInterface[] | any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
  
}
