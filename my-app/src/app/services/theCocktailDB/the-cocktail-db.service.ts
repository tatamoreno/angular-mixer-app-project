import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ResponseApiTheCocktailDBFormattedRandom, ResponseApiTheCocktailDB } from 'src/app/models/models-interface';

@Injectable({
  providedIn: 'root'
})
export class TheCocktailDBService {
  private AlcoholicListUrl = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic';
  private NonAlcoholicListUrl = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic';
  private RandomDrinkUrl = 'https://www.thecocktaildb.com/api/json/v1/1/random.php';

  public RandomDrink: ResponseApiTheCocktailDBFormattedRandom[] = [];

  constructor(private http: HttpClient) { /*empty*/ }

  // RANDOM DRINK:

  public getRandomDrink(): Observable<any> {
    return this.http.get(this.RandomDrinkUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  // // DRINKS LIST:

  // public getDrinksList(url: string): Observable<ResponseApiTheCocktailDB[]> {
  //   return this.http.get(url).pipe(
  //     map((response: ResponseApiTheCocktailDB[] | any) => {
  //       if (!response) {
  //         throw new Error('Value expected!');
  //       } else {
  //         console.log(response);
  //         return response;
  //       }
  //     }),
  //     catchError((err) => {
  //       throw new Error(err.message);
  //     })
  //   );
  // }
}
